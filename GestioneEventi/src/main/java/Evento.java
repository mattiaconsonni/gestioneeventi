import java.util.ArrayList;

public class Evento {

    public enum tipoEvento{
        CONCERTO,
        CONFERENZA,
        FESTIVAL,
        FIERA
    }
    private int id_evento;
    private String nome;
    private tipoEvento evento;
    private String dataEvento;
    private int capienzaMax;
    private String cittaEvento;
    private String provinciaEvento;
    private ArrayList<Utente> utenti;

    public Evento(String nome, tipoEvento evento, String dataEvento, int capienzaMax) {
        this.nome = nome;
        this.evento = evento;
        this.dataEvento = dataEvento;
        this.capienzaMax = capienzaMax;
        this.utenti = new ArrayList<>();
    }


    public int getId_evento() {
        return id_evento;
    }

    public void setId_evento(int id_evento) {
        this.id_evento = id_evento;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public tipoEvento getEvento() {
        return evento;
    }

    public void setEvento(tipoEvento evento) {
        this.evento = evento;
    }

    public String getDataEvento() {
        return dataEvento;
    }

    public void setDataEvento(String dataEvento) {
        this.dataEvento = dataEvento;
    }

    public int getCapienzaMax() {
        return capienzaMax;
    }

    public void setCapienzaMax(int capienzaMax) {
        this.capienzaMax = capienzaMax;
    }

    public String getTipologia() {
        switch (this.evento) {
            case CONCERTO:
                return "Concerto";
            case FIERA:
                return "Fiera";
            case CONFERENZA:
                return "Conferenza";
            case FESTIVAL:
                return "Festival";
            default:
                return null;
        }
    }
    public ArrayList<Utente> getUtenti() {
        return utenti;
    }

    public String getCittaEvento() {
        return cittaEvento;
    }

    public void setCittaEvento(String cittaEvento) {
        this.cittaEvento = cittaEvento;
    }

    public String getProvinciaEvento() {
        return provinciaEvento;
    }

    public void setProvinciaEvento(String provinciaEvento) {
        this.provinciaEvento = provinciaEvento;
    }

    public void setUtenti(ArrayList<Utente> utenti) {
        this.utenti = utenti;
    }
}
