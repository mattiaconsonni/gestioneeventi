import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Locale;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class GestioneEventi {

    private String url = "jdbc:mysql://localhost:3306/db_gestione_eventi";
    private String user = "root";
    private String password = "root";
    private ArrayList<Utente> utenti;
    private ArrayList<Evento> eventi;
    private ArrayList<Location> locations;
    private static final String DB_URL = "jdbc:mysql://localhost:3306/db_gestione_eventi";
    private static final String DB_USER = "root";
    private static final String DB_PASSWORD = "rootroot";



    public boolean addUtente(Utente utente) {
        if (utente != null) {
            utenti.add(utente);
            try (Connection conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD)) {
                String sql = "INSERT INTO PARTECIPANTE (codice_fiscale, nome, cognome, telefono) VALUES (?, ?, ?, ?)";
                try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
                    pstmt.setString(1, utente.getCodiceFiscale());
                    pstmt.setString(2, utente.getNome());
                    pstmt.setString(3, utente.getCognome());
                    pstmt.setString(4, utente.getTelefono());
                    pstmt.executeUpdate();
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }

            return true;
        }
        return false;
    }

    public boolean addEvento(Evento evento) {
        String insert = "INSERT INTO evento (nome, tipologia, capienza, data_evento, nome_localita, provincia_localita) VALUES (?, ?, ?, ?, ?, ?)";
        if (evento != null) {
            try {
                Class.forName("com.mysql.cj.jdbc.Driver");
                Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement preparedStatement = connection.prepareStatement(insert);
                preparedStatement.setString(1, evento.getNome());
                preparedStatement.setString(2, evento.getTipologia());
                preparedStatement.setInt(3, evento.getCapienzaMax());
                preparedStatement.setString(4, evento.getDataEvento());
                preparedStatement.setString(5, evento.getCittaEvento());
                preparedStatement.setString(6, evento.getProvinciaEvento());
                int rowsInserted = preparedStatement.executeUpdate();
                preparedStatement.close();
                connection.close();
            } catch (ClassNotFoundException | SQLException e) {
                System.err.println("Error inserting user!");
                e.printStackTrace();
            }
            eventi.add(evento);
            return true;
        }
        return false;
    }

    public Utente ricercaUtente(String codiceFiscale) {
        for (Utente utente: this.utenti) {
            if (utente.getCodiceFiscale().equals(codiceFiscale))
                try (Connection conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD)) {
                    String sql = "SELECT * FROM PARTECIPANTE WHERE codice_fiscale = ?";
                    try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
                        pstmt.setString(1, utente.getCodiceFiscale());
                        pstmt.executeUpdate();
                    }
                }
                return utente;
        }
        return null;
    }

    public Evento ricercaEvento(String nome) {
        for (Evento evento: this.eventi) {
            if (evento.getNome().equals(nome))
                return evento;
        }
        return null;
    }

    public boolean addLocation(Location location) {
        String insert = "INSERT INTO localita (nome, provincia, stato, indirizzo) VALUES (?, ?, ?, ?)";
        if (location != null) {
            try {
                Class.forName("com.mysql.cj.jdbc.Driver");
                Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement preparedStatement = connection.prepareStatement(insert);
                preparedStatement.setString(1, location.getNomeLocation());
                preparedStatement.setString(2, location.getProvinciaLocation());
                preparedStatement.setString(3, location.getStatoLocation());
                preparedStatement.setString(4, location.getIndirizzo());
                int rowsInserted = preparedStatement.executeUpdate();
                preparedStatement.close();
                connection.close();
            } catch (ClassNotFoundException | SQLException e) {
                System.err.println("Error inserting user!");
                e.printStackTrace();
            }
            locations.add(location);
            return true;
        }
        return false;
    }


    public void removeUtente(Utente utente) {
        utenti.remove(utente);
        try (Connection conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD)) {
            String sql = "DELETE FROM PARTECIPANTE WHERE codice_fiscale=?";
            try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
                pstmt.setString(1, utente.getCodiceFiscale());
                pstmt.executeUpdate();
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void removeEvento(Evento evento) {
        eventi.remove(evento);
    }

    public void removeLocation(Location location) {
        locations.remove(location);
    }

    public boolean addUtenteToEvento(Utente utente, Evento evento) {
        if (evento != null && utente != null) {
            for (Evento e : eventi) {
                if (e.equals(evento)) {
                    if (e.getUtenti().size() < e.getCapienzaMax()) {
                        e.getUtenti().add(utente);
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public boolean removeUtenteFromEvento(Utente utente, Evento evento) {
        if (evento != null && utente != null) {
            for (Evento e : eventi) {
                if (e.equals(evento)) {
                    e.getUtenti().remove(utente);
                    return true;
                }
            }
        }
        return false;
    }

}
