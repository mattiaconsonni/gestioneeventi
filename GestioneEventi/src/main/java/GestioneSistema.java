import java.util.Scanner;

public class GestioneSistema {
    public static void main(String[] args) {
        GestioneEventi gestioneEvento = new GestioneEventi();
        Scanner in = new Scanner(System.in);
        String azione;
        System.out.println("Inserire E per aggiungere l'evento" + "\n" + "Inserire L per aggiungere la località" + "\n" + "Inserire Q per uscire dal sistema.");
        do {
            azione = in.nextLine();
            if (azione.equalsIgnoreCase("E")) {
                System.out.println("Inserisci il nome dell'evento");
                String nome = in.nextLine();

                System.out.println("Scegli il tipo di evento");
                Evento.tipoEvento tipoEvento = null;
                for(Evento.tipoEvento tipo : Evento.tipoEvento.values()){
                    System.out.println(tipo.ordinal() + ". " + tipo);
                }
                boolean condizione = false;
                do{
                    System.out.print("Inserisci il numero corrispondente al tipo di evento: ");
                    if (in.hasNextInt()) {
                        int scelta = in.nextInt();
                        if (scelta >= 0 && scelta < Evento.tipoEvento.values().length) {
                            tipoEvento = Evento.tipoEvento.values()[scelta];
                            condizione = true;
                        } else {
                            System.out.println("Scelta non valida. Riprova.");
                        }
                    } else {
                        System.out.println("Input non valido. Inserisci un numero.");
                        in.next(); // Consuma l'input errato
                    }
                }while(!condizione);

                System.out.println("Inserisci la data dell'evento in formato yyyy-MM-dd");
                String data = in.nextLine();

                System.out.println("Inserire la capienza massima dell'evento");
                int capienza = in.nextInt();

                Evento eventoCreato = new Evento(nome, tipoEvento, data, capienza);
                gestioneEvento.addEvento(eventoCreato);

            }
            else if (azione.equalsIgnoreCase("L")) {
                System.out.println("Inserire il nome della location");
                String nomeLocation = in.nextLine();

                System.out.println("Inserire la provincia della location col formato a due lettere");
                String provinciaLocation = in.nextLine();
                provinciaLocation.toUpperCase();

                System.out.println("Inserire lo stato della location");
                String statoLocation = in.nextLine();

                System.out.println("Inserire l'indizzo della location");
                String indirizzo = in.nextLine();

                Location location = new Location(nomeLocation, provinciaLocation, statoLocation, indirizzo);
                gestioneEvento.addLocation(location);

            }
        }
        while (!azione.equalsIgnoreCase("Q"));
    }
}
