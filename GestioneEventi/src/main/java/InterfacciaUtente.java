import java.util.Scanner;

public class InterfacciaUtente {
    public static void main(String[] args) {
        GestioneEventi gestioneEventi = new GestioneEventi();
        Scanner in = new Scanner(System.in);
        System.out.println("Sei già registrato? " +"\n" + "Y/N");
        String registrazione;
        do {
            System.out.println("Inserire: ");
            registrazione = in.nextLine();
        }
        while (!registrazione.equalsIgnoreCase("Y") && !registrazione.equalsIgnoreCase("N"));
        Utente corrente = null;
        if (registrazione.equals("N")) {
            System.out.println("Inserire codice Fiscale: ");
            String codiceFiscale = in.nextLine();
            System.out.println("Inserire nome: ");
            String nome = in.nextLine();
            System.out.println("Inserire cognome: ");
            String cognome = in.nextLine();
            System.out.println("Inserire numero di telefono: ");
            String telefono = in.nextLine();
            corrente = new Utente(codiceFiscale, nome, cognome, telefono);
            gestioneEventi.addUtente(corrente);
        }
        else {
            System.out.println("Inserire il codice fiscale dell'utente per autenticarsi: ");
            String codiceFiscale = in.nextLine();
            if (gestioneEventi.ricercaUtente(codiceFiscale) != null)
                corrente = gestioneEventi.ricercaUtente(codiceFiscale);
            else {
                System.out.println("Non esiste un utente con questo codice fiscale!");
                System.exit(1);
            }
        }
        System.out.println("Vuoi prenotare un evento o rimuovere la tua partecipazione? \n" +
                            "Premere P per prenotare un evento o R per rimuoverlo");
        String azione;
        do {
            System.out.println("Inserire: ");
            azione = in.nextLine();
        }
        while (!azione.equalsIgnoreCase("R") && !azione.equalsIgnoreCase("P"));
        if (azione.equalsIgnoreCase("R")) {
            System.out.println("Inserisci nome dell'evento dal quale rimuovere la partecipazione: ");
            String nomeEvento = in.nextLine();
            Evento evento = null;
            if (gestioneEventi.ricercaEvento(nomeEvento) != null)
                evento = gestioneEventi.ricercaEvento(nomeEvento);
            else {
                System.out.println("Non esiste l'evento specificato!");
                System.exit(2);
            }
            if (corrente.getEventi().contains(evento)) {
                gestioneEventi.removeUtenteFromEvento(corrente, evento);
                System.out.println("Rimozione avvenuta con sucesso!");
            }
            else {
                System.out.println("L'evento a cui fa riferimento non esiste, riprovare in seguito");
            }
        }
        else {
            System.out.println("Inserisci nome dell'evento al quale vuole partecipare: ");
            String nomeEvento = in.nextLine();
            Evento evento = null;
            if (gestioneEventi.ricercaEvento(nomeEvento) != null)
                evento = gestioneEventi.ricercaEvento(nomeEvento);
            else {
                System.out.println("Non esiste l'evento specificato!");
                System.exit(2);
            }
            gestioneEventi.addUtenteToEvento(corrente, evento);
            System.out.println("Ti sei registrato correttamente!");
        }
        System.out.println("Grazie di aver scelto il nostro servizio!");
        in.close();
    }
}
