import java.util.ArrayList;

public class Location {
    private String nomeLocation;
    private String provinciaLocation;
    private String statoLocation;
    private String indirizzo;
    private ArrayList<Evento> eventiOspitati;

    public Location(String nomeLocation, String provinciaLocation, String statoLocation, String indirizzo) {
        this.nomeLocation = nomeLocation;
        this.provinciaLocation = provinciaLocation;
        this.statoLocation = statoLocation;
        this.indirizzo = indirizzo;
        this.eventiOspitati = new ArrayList<>();
    }

    public String getNomeLocation() {
        return nomeLocation;
    }

    public void setNomeLocation(String nomeLocation) {
        this.nomeLocation = nomeLocation;
    }

    public String getProvinciaLocation() {
        return provinciaLocation;
    }

    public void setProvinciaLocation(String provinciaLocation) {
        this.provinciaLocation = provinciaLocation;
    }

    public String getStatoLocation() {
        return statoLocation;
    }

    public void setStatoLocation(String statoLocation) {
        this.statoLocation = statoLocation;
    }

    public String getIndirizzo() {
        return indirizzo;
    }

    public void setIndirizzo(String indirizzo) {
        this.indirizzo = indirizzo;
    }

    public ArrayList<Evento> getEventiOspitati() {
        return eventiOspitati;
    }
}
