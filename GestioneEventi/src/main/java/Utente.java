import java.util.ArrayList;
import java.util.List;

public class Utente {
    private String codiceFiscale;
    private String nome;
    private String cognome;
    private String telefono;
    private ArrayList<Evento> eventi;

    public Utente(String codiceFiscale,String nome, String cognome, String telefono) {
        this.codiceFiscale = codiceFiscale;
        this.nome = nome;
        this.cognome = cognome;
        this.telefono = telefono;
        this.eventi = new ArrayList<>();
    }

    public String getCodiceFiscale() {
        return codiceFiscale;
    }

    public void setCodiceFiscale(String codiceFiscale) {
        this.codiceFiscale = codiceFiscale;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public ArrayList<Evento> getEventi() {
        return eventi;
    }


}


