create database IF NOT EXISTS db_gestione_eventi;

use db_gestione_eventi;

create table IF NOT EXISTS partecipante (
	codice_fiscale varchar(16) primary key,
    nome varchar(20),
    cognome varchar(30),
    telefono varchar(10)
    );
    
create table IF NOT EXISTS localita (
	nome varchar(30),
    provincia varchar(2),
    stato varchar(30),
    indirizzo varchar(50),
    constraint pk_localita primary key(nome, provincia)
    );
    
create table IF NOT EXISTS evento (
	id int primary key auto_increment,
    nome varchar(30),
    tipologia varchar(30),
    capienza int,
    data_evento date,
    nome_localita varchar(30),
    provincia_localita varchar(30),
    constraint fk_evento foreign key (nome_localita, provincia_localita) references localita(nome, provincia)
    );
    
create table IF NOT EXISTS prenotazione (
	codice_fiscale_partecipante varchar(16),
    constraint fk_prenotazione_partecipante foreign key (codice_fiscale_partecipante) references partecipante(codice_fiscale),
    id_evento int,
    constraint fk_prenotazione_evento foreign key (id_evento) references evento(id),
    constraint pk_prenotazione primary key (codice_fiscale_partecipante, id_evento)
    );